RW Docker Template
===========


Development
-----------

**Docker**

Local start: `docker-compose up`

Local stop: `docker-compose stop`


Restart container: `docker-compose restart <CONTAINER_NAME>`

Exec in container: `docker exec -it <CONTAINER_ID> bash`

Delete inactive containers: `docker system prune`

-----------

**Phinx**

Create migration (see in `apps/backend/database/migrations`):

`docker exec -it b_backend php vendor/bin/phinx create <NewMigrationName>`

Migrate:

`docker exec -it b_backend php vendor/bin/phinx migrate -e default`

Seeding:

`docker exec -it b_backend php vendor/bin/phinx seed:run -e default -s <SeedName>`

-----------

**Phalcon dev tools**

`docker exec -it b_backend php vendor/phalcon/devtools/phalcon.php <Commands>`

Example

`docker exec -it b_backend php vendor/phalcon/devtools/phalcon.php info`

-----------

**RabbitMQ**

[Management dashboard](http://localhost:15672)

Create web user:

`docker exec -it b_rabbitmq bash /usr/local/bin/create-web-user.sh`

-----------

**Schedulers**

Scheduler list:

`docker exec -it b_backend php vendor/bin/crunz schedule:list`

-----------
 
**Composer**

Update composer:

`docker exec -it b_backend composer update`
-----------
 
**Manual DB Backup**

Soon...