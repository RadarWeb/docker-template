<?php

//use RadarWeb\Helpers\AssetsManager;
//use RadarWeb\Helpers\RedisCache;
//use RadarWeb\Services\Mailer\MailerService;
//use RadarWeb\Services\RabbitMQ;
//use RadarWeb\Services\SMS;
//use Fabfuel\Prophiler\Aggregator\Cache\CacheAggregator;
//use Fabfuel\Prophiler\Aggregator\Database\QueryAggregator;
//use Fabfuel\Prophiler\DataCollector\Request;
////use Fabfuel\Prophiler\Decorator\Phalcon\Cache\BackendDecorator;
//use Fabfuel\Prophiler\Plugin\Manager\Phalcon;
//use Fabfuel\Prophiler\Profiler;
//use Fabfuel\Prophiler\Toolbar;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as FlashSession;

ini_set('session.name', 'RW_A_T');
ini_set('session.cookie_httponly', '1');
ini_set('session.cookie_secure', '1');

//$container->set('config', $config);

$di->set('url', function () use ($config) {
	$url = new UrlProvider();
	$url->setBaseUri($config->url);
	return $url;
});

$di->set('session', function () {
	$session = new SessionAdapter();
	$session->start();
	return $session;
});

$di->set('db', function () use ($config) {

	$cfg = [
		"host" => $config->database->host,
		"username" => $config->database->username,
		"password" => $config->database->password,
		"dbname" => $config->database->dbname,
	];

	$dbClass = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;

	return new $dbClass($cfg);

});

$di->set('flash', function () {
	return new FlashSession([
		'error' => 'alert alert-danger',
		'success' => 'alert alert-success',
		'notice' => 'alert alert-info',
		'warning' => 'alert alert-warning'
	]);
});

/*
 * Profiler
 */

$profiler = null;

/*if(@$runProphiler){

	$profiler = new Profiler();

	$profiler->addAggregator(new QueryAggregator());
	$profiler->addAggregator(new CacheAggregator());

	$pluginManager = new Phalcon($profiler);
	$pluginManager->register();

	$toolbar = new Toolbar($profiler);
	$toolbar->addDataCollector(new Request());

	$container->setShared('profiler', $profiler);
	$container->setShared('toolbar', $toolbar);
}*/

/*$container->set('cache', function() use ($config, $profiler){

	$frontCache = new FrontendData(["lifetime" => 300]);

	$cache = new RedisCache($frontCache, [
		"host"       => $config->redis->host,
		"auth"       => $config->redis->password,
		"port"       => $config->redis->port,
		"persistent" => true,
		"statsKey"   => 'N4gc4u',
		'index'      => 2,
	]);

	if(!is_null($profiler)){
		return new BackendDecorator($cache, $profiler);
	}else{
		return $cache;
	}
}, !CLI);

$container->set('modelsMetadata', function() use ($config){

	if($config->debug){
		return new \Phalcon\Mvc\Model\Metadata\Memory();
	}

	return new RedisMetaData(
		[
			"statsKey"   => "5sFq?3",
			"host"       => $config->redis->host,
			"auth"       => $config->redis->password,
			"port"       => $config->redis->port,
			"persistent" => true,
			"lifetime"   => 172800,
			'index'      => 2,
		]
	);
}, true);

$container->set('session', function() use ($config){

	$session = new RedisSession(
		[
			"uniqueId"   => "Pv[m5K",
			"host"       => $config->redis->host,
			"auth"       => $config->redis->password,
			"port"       => $config->redis->port,
			"persistent" => true,
			'index'      => 1,
		]
	);

	$session->start();

	return $session;
});*/

/**
 * Custom AssetsManager
 */
/*$container->set("assets", function(){
	return new AssetsManager();
});*/

/*$container->set('rabbitmq', function() use ($config){
	return new RabbitMQ(
		$config->rabbitmq->host,
		$config->rabbitmq->port,
		$config->rabbitmq->login,
		$config->rabbitmq->password
	);
});

$container->set('logger', function() use ($config){
	return new \BurgerKing\Services\Logger();
}, true);

$container->set('sms', function() use ($config){

	$sms = new SMS(
		$config->sms->host,
		$config->sms->api,
		$config->sms->version,
		$config->sms->service,
		$config->sms->name,
		$config->sms->username,
		$config->sms->password
	);

	return $sms;
});

$container->set('mailer', function() use ($config){
	$service = new MailerService($config->mail->address);

	return $service->getMailer();
});*/