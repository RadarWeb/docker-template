<?php

use Phalcon\Loader;

$loader = new Loader();

if(isset($modules)) foreach($modules as $key => $module){

	$path = str_replace('/Module.php', '', $module['path']);

	$loader->registerNamespaces([
		$module['namespace'] => $path
	], true);

}

foreach ($config->application->namespaces as $key => $value) {
	$loader->registerNamespaces([
		$key => $value
	], true);
}

foreach ($config->application->dirs as $value) {
	$loader->registerDirs([
		$value
	], true);
}

$loader->register();

return $loader;