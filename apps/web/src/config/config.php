<?php

$config = new \Phalcon\Config([

	'env' => 'local', // local, stage, stage

	'debug' => true,

	'logging' => true,

	'profiler' => true,

	'url'       => 'http://localhost/',

	'image_url' => 'localhost/',

	'api_host' => 'localhost',

	/*'database' => [
		'host'     => 'mysql',
		'dbname'   => 'rw_database',
		'username' => 'rw_user',
		'password' => 'rw_pass',
	],*/

	'database' => [
		'adapter'  => 'pgsql',
		'host'     => 'postgres',
		'dbname'   => 'rw_database',
		'username' => 'rw_user',
		'password' => 'rw_pass',
	],

	'application' => [
		'namespaces' => [
			'RadarWeb' => __DIR__ . '/../',

			'RadarWeb\Models'               => __DIR__ . '/../models/',
			'RadarWeb\Models\Events'        => __DIR__ . '/../models/events/',

//			'RadarWeb\Momentos'                   => __DIR__ . '/../momentos/',
//			'RadarWeb\Momentos\Models'            => __DIR__ . '/../momentos/models/',

//			'RadarWeb\Services'          => __DIR__ . '/../services/',
//			'RadarWeb\Helpers'           => __DIR__ . '/../helpers/',
//			'RadarWeb\Libs'              => __DIR__ . '/../libs',

//			'RadarWeb\Cli'            => __DIR__ . '/../cli/',
//			'RadarWeb\Cli\Tasks'      => __DIR__ . '/../cli/tasks/',
//			'RadarWeb\Cli\Schedulers' => __DIR__ . '/../cli/schedulers/',

//			'RadarWeb\Enums' => __DIR__ . '/../enums/',
		],

		'dirs' => [],

		'token_name'     => 'X-Radar-Web-Token',
		'api_token_name' => 'X-Radar-Web-Api',
	],

]);


/**
 * Merge with local config
 */

if(file_exists(__DIR__ . '/env/config.local.php')){
	$config->merge(require __DIR__ . '/env/config.local.php');
}

/**
 * Merge with env config
 */

$config_mode = $config->get('env');
if($config_mode !== 'local'){
	$config_fname = __DIR__ . "/env/config.$config_mode.php";
	if(file_exists($config_fname)){
		/** @noinspection PhpIncludeInspection */
		$config->merge(require $config_fname);
	}
}

return $config;