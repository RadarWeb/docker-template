<?php

use Phalcon\Debug;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Application as BaseApplication;

class Bootstrap extends BaseApplication {

	protected function registerServices() {

		define('CLI', false);

		ini_set('memory_limit', '1024M');

		require __DIR__ . '/defines.php';

		require ROOT_PATH . 'vendor/autoload.php';

		$config = require __DIR__ . '/config.php';

		if (@$config->debug) {

			error_reporting(E_ALL);
			ini_set("display_errors", 1);
			ini_set("display_startup_errors", 1);
			ini_set("html_errors", 1);
			ini_set("log_errors", 1);

			$debug = new Debug();

			$debug->listen();

			$_SERVER['start_time'] = microtime(true);
			$_SERVER['start_memory'] = memory_get_usage();

		}

		$di = new FactoryDefault();

		$di->set('config', $config);

		$loader = require __DIR__ . '/loader.php';

		$di->set('router', function () {
			return require __DIR__ . '/router.php';
		});

		require __DIR__ . '/services.php';

		$this->setDI($di);
	}

	public function main() {

		$this->registerServices();

		$this->registerModules(require __DIR__ . '/modules.php');

		echo $this->handle()->getContent();

	}

}

$application = new Bootstrap();

try {

	$application->main();

} catch (Exception $exception) {

	if (@$config->debug) echo '<table>' . $exception->getMessage() . '</table>';

}