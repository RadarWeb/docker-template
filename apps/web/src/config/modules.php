<?php

return [

	'admin' => [
		'namespace' => 'RadarWeb\Modules\Admin',
		'className' => 'RadarWeb\Modules\Admin\Module',
		'path'      => __DIR__ . '/../modules/admin/Module.php',
	],

	'front' => [
		'namespace' => 'RadarWeb\Modules\Front',
		'className' => 'RadarWeb\Modules\Front\Module',
		'path'      => __DIR__ . '/../modules/front/Module.php',
	],

	/*'v1' => [
		'namespace'     => 'RadarWeb\Modules\Api\V1',
		'className'     => 'RadarWeb\Modules\Api\V1\Module',
		'path'          => __DIR__ . '/../modules/api/v1/Module.php',
	],*/

];