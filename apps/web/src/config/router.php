<?php

use Phalcon\Mvc\Router;

$router = new Router();
$router->setDefaultModule("front");

$router->add('/:controller/:action/:params', [
	'module' => 'front',
	'controller' => 1,
	'action' => 2,
	'params' => 3,
]);

$router->add("/admin", [
	'module' => 'admin',
	'controller' => 'index',
	'action' => 'index',
]);

$router->add('/admin/:controller/:action/:params', [
	'module' => 'admin',
	'controller' => 1,
	'action' => 2,
	'params' => 3,
]);

$router->add('/admin/:controller', [
	'module' => 'admin',
	'controller' => 1,
	'action' => 'index'
]);

/*$router->add("/api/v1", [
	'module'     => 'v1',
	'controller' => 'index',
	'action'     => 'index',
]);

$router->add('/api/v1/:controller/:action/:params', array(
	'module' => 'v1',
	'controller' => 1,
	'action' => 2,
	'params' => 3,
));

$router->add('/api/v1/:controller', array(
	'module' => 'v1',
	'controller' => 1,
	'action' => 'index'
));*/

return $router;