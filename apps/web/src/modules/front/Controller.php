<?php
/**
 * Developer: Andrew Karpich
 * Date: 22.06.2017 16:35
 */

namespace RadarWeb\Modules\Front;

/**
 * @property \Phalcon\Config                $config
 */
class Controller extends \Phalcon\Mvc\Controller {

    protected $benchmark;

    protected function initialize(){

        $this->tag->prependTitle('Radar Web® | ');

        $controller = $this->router->getControllerName() ? $this->router->getControllerName() : 'index';
        $action = $this->router->getActionName() ? $this->router->getActionName() : 'index';

        $mainPath = '/front/' . $controller . '/' . $action;

//        $compiledAssetsPath = $this->config->cache_admin_root_path . 'public/cache/assets/admin';
//
//        $styles = $this->assets->collection('styles')
//            ->setTargetPath($compiledAssetsPath . '/styles.min.css')
//            ->setSourcePath(ROOT_PATH . 'public/')
//            ->setTargetUri('/cache/assets/admin/styles.min.css')
//            ->addCss('fonts/fontawesome/css/font-awesome.css')
//            ->addCss('libs/bootstrap/dist/css/bootstrap.css')
//            ->addCss('libs/bootstrap/dist/css/bootstrap-responsive.css')
//            ->addCss('libs/jquery-ui/themes/base/all.css')
//            ->addCss('styles/style.css')
//            ->addCss('libs/toastr/build/toastr.min.css', true, false);
//
//        $scripts = $this->assets->collection('scripts')
//            ->setTargetPath($compiledAssetsPath . '/scripts.min.js')
//            ->setSourcePath(ROOT_PATH . 'public/')
//            ->setTargetUri('/cache/assets/admin/scripts.min.js')
//            ->addJs('libs/jquery/dist/jquery.min.js')
//            ->addJs('libs/jquery-ui/jquery-ui.min.js')
//            ->addJs('libs/bootstrap/dist/js/bootstrap.min.js')
//            ->addJs('libs/toastr/build/toastr.min.js');


        $footerPageScripts = $this->assets->collection('page_scripts');
        if(file_exists(ROOT_PATH . 'public/assets' . $mainPath . '.js')){
            $footerPageScripts->addJs('assets' . $mainPath . '.js' . ( $this->config->debug ? '?t=' . time() : ''));
        }

        $page_styles = $this->assets->collection('page_styles');
        if(file_exists(ROOT_PATH . 'public/assets' . $mainPath . '.css')){
            $page_styles->addCss('assets' . $mainPath . '.css' . ( $this->config->debug ? '?t=' . time() : ''));
        }


        if($this->request->has('debug')){

            $this->benchmark = $this->profiler->start($this->dispatcher->getControllerName(), [
                'Action' => $this->dispatcher->getActionName(),
            ], 'Controller');

        }

    }

    protected function sendJson($data){

        $this->view->disable();

        $this->response->setContentType('application/json', 'UTF-8');

        $this->response->setJsonContent($data);

        $this->response->send();

        exit;
    }


    protected function sendXml($data){

        $this->view->disable();

        $this->response->setContentType('application/xml', 'UTF-8');

        $this->response->setContent($data);

        $this->response->send();

        exit;
    }

}
