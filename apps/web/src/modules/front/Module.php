<?php

namespace RadarWeb\Modules\Front;

use Carbon\Carbon;
use Phalcon\DiInterface;
use Phalcon\Events\Manager;
use Phalcon\Http\Response;
use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;

class Module implements ModuleDefinitionInterface {

    public function registerAutoloaders(DiInterface $di = null){

        $loader = new Loader();

        $loader->registerNamespaces([
            __NAMESPACE__ => __DIR__,
            __NAMESPACE__ . '\Controllers' => __DIR__ . '/controllers/',
	        __NAMESPACE__ . '\Forms' => __DIR__ . '/forms/',
            __NAMESPACE__ . '\Plugins'     => __DIR__ . '/plugins/',
        ]);

        $loader->register();

    }

    public function registerServices(DiInterface $di){

	    $di->set('dispatcher', function () {

		    $dispatcher = new Dispatcher();

		    $eventManager = new Manager();

		    $dispatcher->setEventsManager($eventManager);
		    $dispatcher->setDefaultNamespace('RadarWeb\Modules\Front\Controllers\\');
		    return $dispatcher;

	    });

	    $di->set("volt", function($view, $di) {
		    $volt = new VoltEngine($view, $di);
		    $volt->setOptions([
			    "compiledPath" => function($templatePath) {
				    $dir = ROOT_PATH . 'cache/volt/front/';
				    if(!is_dir($dir)) {
					    mkdir($dir, 0755, true);
				    }
				    return $dir . md5($templatePath) . '.compiled';
			    },
		    ]);

		    $compiler = $volt->getCompiler();
		    $compiler->addFunction('substr', 'mb_substr');

		    return $volt;
	    });

	    $di->set("view", function() {
		    $view = new View();

		    $view->setViewsDir(__DIR__ . '/views/');
		    $view->registerEngines([
			    ".volt" => "volt",
		    ]);

		    return $view;
	    });

	    /**
	     * Register the flash service with custom CSS classes
	     */
	    $di->set('flash', function () {
		    return new FlashSession(array(
			    'error' => 'alert alert-danger',
			    'success' => 'alert alert-success',
			    'notice' => 'alert alert-info',
			    'warning' => 'alert alert-warning'
		    ));
	    });

	    $di->set('carbon', function(){
		    return new Carbon();
	    });

        /**
         * @var Response $response
         */
        $response = $di->get('response');

        $response->setHeader('X-XSS-Protection', '1;mode=block');
        $response->setHeader('X-Content-Type-Options', 'nosniff');

    }

}