<?php

use Phalcon\Config;

require __DIR__ . '/src/config/defines.php';

/**
 * @var Config $config
 */
$config = require CONFIG_PATH . 'config.php';

return [
    'paths'          => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/database/migrations',
        'seeds'      => '%%PHINX_CONFIG_DIR%%/database/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database'        => 'default',
	    'default'                 => [
		    'adapter'   => 'pgsql',
		    'host'      => 'postgres',
		    'name'      => 'rw_database',
		    'user'      => 'rw_user',
		    'pass'      => 'rw_pass',
		    'port'      => '5432',
		    'charset'   => 'utf8',
		    'collation' => 'utf8_unicode_ci',
	    ],
        /*'default'                 => [
            'adapter'   => 'mysql',
            'host'      => 'mysql',
            'name'      => 'rw_database',
            'user'      => 'rw_user',
            'pass'      => 'rw_pass',
            'port'      => '3306',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],*/
    ],
];