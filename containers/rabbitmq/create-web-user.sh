#!/bin/bash

rabbitmqctl add_user web AmL25D4Z;
rabbitmqctl set_user_tags web manager;
rabbitmqctl add_vhost /web;
rabbitmqctl set_permissions -p /web web ".*" ".*" ".*";